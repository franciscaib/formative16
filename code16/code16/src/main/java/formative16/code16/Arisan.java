package formative16.code16;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Arisan {
	Map<LocalTime, Member> daftarMemberDapat = new HashMap<>();
//	Map<LocalDate, Member> daftarMemberDapat = new HashMap<>();
	ArrayList<Member> daftarMember = new ArrayList<>();
	
	public void addMember(Member member) {
		daftarMember.add(member);
	}
	
	public Member getArisan() {
		Random r = new Random();
        int rand = r.nextInt(daftarMember.size());
        for (int i = 0; i < daftarMember.size(); i++) {
			if(i == rand) {
				LocalTime waktuDapat = LocalTime.now();
//				LocalDate waktuDapat = LocalDate.now();
				daftarMemberDapat.put(waktuDapat, daftarMember.get(i));
				daftarMember.remove(i);
				return daftarMemberDapat.get(waktuDapat);
			}
		}
        return null;
	}
	
	public void printMember() {
		for (int i = 0; i < daftarMember.size(); i++) {
			System.out.println((i+1)+". "+daftarMember.get(i).getNama()+"\t"+
					daftarMember.get(i).getCode()+"\t"+
					daftarMember.get(i).getIuran());
		}
	}
	
	public void printMemberDapat() {
		int i = 0;
		for (Map.Entry<LocalTime,Member> entry : daftarMemberDapat.entrySet()) {
			i ++;
			LocalTime key = entry.getKey();
//			LocalDate key = entry.getKey();
			Member val = entry.getValue();
			System.out.println(i+". "+key+"\t"+val.getNama()+"\t"+val.getIuran());
		}
	}
}
