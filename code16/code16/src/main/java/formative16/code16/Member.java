package formative16.code16;

public class Member {
	private String nama;
	private int code;
	private int iuran;
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public int getIuran() {
		return iuran;
	}
	public void setIuran(int iuran) {
		this.iuran = iuran;
	}
	
	
}
