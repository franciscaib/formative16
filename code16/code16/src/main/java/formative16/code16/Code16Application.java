package formative16.code16;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.format.datetime.joda.LocalDateParser;

@SpringBootApplication
public class Code16Application {

	public static void main(String[] args) {
//		SpringApplication.run(Code16Application.class, args);
		ArrayList<Integer> tempCode = new ArrayList<Integer>();
		Arisan arisan = new Arisan();
		Scanner sc = new Scanner(System.in);
		int tagihan = 0;
		
		boolean loop= true;
		while(loop) {
			System.out.println("\n---Menu Aplikasi---");
			System.out.println("1. Tambah member");
			System.out.println("2. Jalankan arisan");
			System.out.println("3. Bayar arisan");
			System.out.println("4. Lihat daftar member");
			System.out.println("5. Lihat daftar member dapat");
			System.out.println("6. Keluar");
			
			System.out.print("Masukan pilihan menu : ");
			int masukan = sc.nextInt();
			System.out.println();
			
			switch (masukan) {
				case 1:
					if(arisan.daftarMember.size() < 15) {
						Member member = new Member();
						System.out.print("Masukan nama : ");
						member.setNama(sc.next());
						int rand;
						do {
							Random r = new Random();
							rand = r.nextInt(15);
							
						}while(tempCode.contains(rand));
						tempCode.add(rand);
						member.setCode(rand);
						member.setIuran(0);
						arisan.addMember(member);	
					}else {
						System.out.println("Jumlah member arisan sudah penuh");
					}
					break;
					
				case 2:
					if(arisan.daftarMember.size() < 15 && arisan.daftarMemberDapat.size() == 0) {
						System.out.println("Maaf peserta arisan masih kurang");
					}else if(arisan.daftarMember.size() == 0 && arisan.daftarMemberDapat.size() == 15) {
						System.out.println("Semua member sudah mendapatkan arisan");
					}else if(tagihan < 2250000) {
						System.out.println("Maaf uang arisan belum cukup");
					}else {
						Member m = arisan.getArisan();
						LocalTime time = LocalTime.now();
//						LocalDate time = LocalDate.now();
						System.out.println("Arisan pada "
								+ time
								+ " diperoleh "+ m.getNama());
						tagihan = 0;
						for (int j = 0; j < arisan.daftarMember.size(); j++) {
							arisan.daftarMember.get(j).setIuran(0);
						}
						for (Entry<LocalTime, Member> entry : arisan.daftarMemberDapat.entrySet()) {
							entry.getValue().setIuran(0);
						}
					}
					break;
				case 3:
					if(arisan.daftarMember.size() != 0 || arisan.daftarMemberDapat.size() != 0) {
						System.out.print("Masukan kode member : ");
						int masuk = sc.nextInt();
						for (int j = 0; j < arisan.daftarMember.size(); j++) {
							if(arisan.daftarMember.get(j).getCode() == masuk) {
								if(arisan.daftarMember.get(j).getIuran() == 0) {
									System.out.print("Masukan jumlah nominal : ");
									int uang = sc.nextInt();
									if(uang < 150000) {
										System.out.println("Maaf uang anda kurang");
									}else {
										if(uang > 150000) {
											System.out.println("Kembalian uang anda " + (uang-150000));
										}
										arisan.daftarMember.get(j).setIuran(uang);
										System.out.println("Terimakasih sudah membayar arisan");
										tagihan += 150000;
									}
									break;
								}else {
									System.out.println("Anda tidak memiliki tagihan");
								}
								
							}
						}
						for (Entry<LocalTime, Member> entry : arisan.daftarMemberDapat.entrySet()) {
							LocalTime key = entry.getKey();
							Member val = entry.getValue();
							if(val.getCode() == masuk) {
								if(val.getIuran() == 0) {
									System.out.println("Masukan jumlah nominal : ");
									int uang = sc.nextInt();
									if(uang < 150000) {
										System.out.println("Maaf uang anda kurang");
									}else {
										if(uang > 150000) {
											System.out.println("Kembalian uang anda " + (uang-150000));
										}
										val.setIuran(uang);
										System.out.println("Terimakasih sudah membayar arisan");
										tagihan += 150000;
									}
									break;

								}else {
									System.out.println("Anda tidak memiliki tagihan");
								}
							}
						}
					}else {
						System.out.println("Member kosong");
					}
					break;
				case 4:
					if(arisan.daftarMember.size() == 0 && arisan.daftarMemberDapat.size() == 0) {
						System.out.println("Belum ada member");
					}else if(arisan.daftarMember.size() == 0 && arisan.daftarMemberDapat.size() == 15) {
						System.out.println("Arisan sudah selesai");
					}else {
						System.out.println("Daftar member arisan");
						System.out.println("No. Nama\tKode\tIuran");
						arisan.printMember();
						
					}
					break;
				case 5:
					if(arisan.daftarMemberDapat.size() == 0) {
						System.out.println("Arisan belum dimulai");
					}else {
						System.out.println("Daftar member arisan");
						System.out.println("No. Tanggal\tNama\tIuran");
						arisan.printMemberDapat();
					}
					break;
				case 6:
					loop = false;
					break;
			}
		}
	}

}
